package com.example.signupform;


import java.io.Serializable;

public class User implements Serializable{
    private String mUserName;
    private String mPassword;

    User(String username, String password)
    {
        this.mUserName = username;
        this.mPassword = password;
    }

    public String getUserName() {
        return mUserName;
    }
}
